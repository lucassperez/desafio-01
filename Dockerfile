FROM ruby:2.7.1

RUN apt-get -y update
RUN apt-get -y install redis-server
RUN mkdir /ibge
WORKDIR /ibge
ADD Gemfile /ibge/Gemfile
ADD Gemfile.lock /ibge/Gemfile.lock
RUN bundle install
ADD . /ibge
