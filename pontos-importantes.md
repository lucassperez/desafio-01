Coisas a se fazer/pensar, ideias/plano do projeto
#### Arquivo README
Pré requisitos
- O que preciso instalar? Ruby? O que mais?

Configuração
- Que comandos rodar? Aonde rodar?

Como executar os testes\
Quais funcionalidades existem na aplicação?\

Versão do Ruby no Gemfile\

#### Classes como "models"
Nome, cidade, estado...\
Ter uma classe Base e usar herança para reproduzir
comportamentos comuns\
Manter a responsabilidade de tratar regras de negócio
- Que classes devem imprimir tabelas?
- Que classes devem consumir dados de fora?

**Grupos de classes?**

1) Classe para execução
- Classe "Application" ("ligar o programa")
- Application.run para iniciar o código

2) Classes para consumo da API
- Ter uma classe base com métodos para obter e tratar dados
- Herdar os comportamentos em classes específicas de acordo
com o escopo

3) Classes para renderizar as "telas"
- As tabelas são sempre similares, por que não criar uma classe?
- Table.new(dados).render()

**Ver algo sobre autoload? Rails guides ajuda**

**Como criar seu próprio MVC em uma app terminal com Ruby puro?**


#### Extrair toda a lógica de banco de dados num canto separado
Foi a dica do Allan da Rebase\
Se fizer isso, é fácil mudar de banco de dados depois
(SQLite pro Postgres, por exemplo)
- Fazer uma classe específica pra tratar da comunicação com o
banco de dados? Seria uma classe "base"?

Acho que aqui tem que reaproveitar código ao máximo pra só ter que
ir num ponto mudar quando tiver que mudar (isso me parece uma
excelente ideia sempre, na verdade)

### Docker
Desafio 1: Criar um Dockerfile para o projeto\
Desafio 2: Criar um docker-compose e separar sua app do banco de dados

### Banco de dados
Por enquanto há somente duas tabelas, uma com os estados e outra com as cidades
Podia ter uma tabela city_rank e state_rank, algo assim:\
**city_rank**\
locale ---|masc_1|masc_1_pop|masc_2|masc_2_pop
___
4205407| JOAO | ----- 6065 ---- | JOSE | 5215\
4209102| JOAO | ----- 7312 ---- | JOSE | 7301\
3550308| JOSE | ---- 268458 --- | JOAO | 128977\
E daí com um join entre a tabela city_rank e cities conseguiria saber qual cidade que é, de que estado etc.
