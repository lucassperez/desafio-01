# frozen_string_literal: true

require_relative 'load_files'

ENV['db'] ||= 'production'

UF_LIST = 1
NAMES_BY_UF = 2
NAMES_BY_CITY = 3
NAMES_BY_DECADES = 4
SAVE_RANK_IN_DB = 9
EXIT = 0

Application.new.run
