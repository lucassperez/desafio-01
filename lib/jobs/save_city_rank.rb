# frozen_string_literal: true

require 'sidekiq'

require 'json'
require 'faraday'
require 'pg'
require_relative '../api_check'
require_relative '../database'

# Sidekiq worker to do async jobs
class SaveCityRankWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    db = Database.new
    response = ApiCheck.new.rank_by_locale(id)
    # response is a hash with the keys :geral, :male and :fem (categories)
    # and each of them is another hash with the keys :localidade, :sexo and :res
    # localidade is a string with the id of the place and res is an array with 20 elements,
    # where each one of those elements is a hash with the keys :nome, :frequencia and :ranking
    # Basically, we work with response[category][:res]
    response.each_key do |category|
      columns = create_columns(response[category])
      puts " (\e[93mcities_rank_#{category}\e[0m)\tcity_id: #{columns[:city_id]}\n"
      db.upsert(table: "cities_rank_#{category}", columns: columns, query: { city_id: id })
    end
  end

  private

  def create_columns(category)
    hash = { city_id: category[:localidade], state_id: category[:localidade][0...2] }
    category[:res].each do |name|
      next if name[:ranking] >= 21

      hash["name#{name[:ranking]}"] = name[:nome]
      hash["frequency#{name[:ranking]}"] = name[:frequencia]
    end
    hash
  end
end
