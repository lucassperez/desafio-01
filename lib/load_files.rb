# frozen_string_literal: true

# It is handy to be able to load all the
# classes inside irb for testing purposes

require 'faraday'
require 'json'
require 'pg'
require 'sidekiq'

require_relative 'api_check'
require_relative 'application'
require_relative 'database'
require_relative 'db_check'
require_relative 'printer'

require_relative 'jobs/save_city_rank'
