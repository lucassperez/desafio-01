# frozen_string_literal: true

# This class visits the APIs and manages the responses
class ApiCheck
  def api_response(url)
    response = Faraday.get(url)

    return ['Erro'] if response.status != 200

    JSON.parse(response.body, symbolize_names: true)
  end

  def list_all_states
    api_response("#{url_locales}estados")
  end

  def rank_by_locale(locale)
    response = {}
    response[:geral] = api_response("#{url_names}ranking?localidade=#{locale}")[0]
    response[:male] = api_response("#{url_names}ranking?localidade=#{locale}&sexo=M")[0]
    response[:fem] = api_response("#{url_names}ranking?localidade=#{locale}&sexo=F")[0]
    response.value?(['Erro']) ? nil : response
  end

  def names_by_city(city)
    city_info = city.first
    if city_info.nil?
      puts "\n\e[95mNão conseguimos localizar esta cidade. ):\e[0m"
      puts 'O servidor pode estar fora do ar ou talvez tenha tido algum erro de digitação.'
      return nil
    end
    { **city_info, rank: rank_by_locale(city_info[:id]) }
  end

  def names_by_decades(names)
    api_response("#{url_names}#{names}")
  end

  def find_city(city)
    state_id, city_name = city_name_and_state_id(city)
    url = url_locales
    url += state_id.nil? ? 'municipios' : "estados/#{state_id}/municipios"
    response = api_response(url)
    response.each { |item| return { id: item[:id], nome: item[:nome] } if item[:nome].upcase == city_name }
    nil
  end

  def all_cities_of_a_state(id)
    api_response("#{url_locales}estados/#{id}/municipios")
  end

  def find_state_id(query)
    query = query.upcase.strip
    response = api_response("#{url_locales}estados")
    response.each { |state| return state[:id] if state[:sigla] == query || state[:id] == query.to_i }
    nil
  end

  private

  def url_names
    'https://servicodados.ibge.gov.br/api/v2/censos/nomes/'
  end

  def url_locales
    'https://servicodados.ibge.gov.br/api/v1/localidades/'
  end

  def city_name_and_state_id(city)
    state_id = find_state_id(city.split('/').last) if city.include?('/')
    city_name = city.split('/').first.upcase.strip
    [state_id, city_name]
  end
end
