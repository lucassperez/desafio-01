# frozen_string_literal: true

# This class prints tables
class Printer
  def print_all_states(response)
    puts("\e[0;4;7m ID | SIGLA | NOME \e[0m")
    response.each do |state|
      puts(" #{state[:id]} |  #{state[:sigla]}   | #{state[:nome]}")
    end
  end

  def print_names_by_state(state, response)
    puts "\e[0m\n\tO estado escolhido foi \e[92m#{state[:nome]} (#{state[:sigla]})\e[0m!"
    puts "\nA população de \e[93m#{state[:nome]}\e[0m em 2019 era de
          \e[93m#{state[:population2019]}\e[0m habitantes."
    print_table(state, response)
  end

  def print_names_by_city(response)
    puts "\e[0m\n\tA cidade escolhida foi \e[92m#{response[:nome]}\e[0m!"
    puts "\nA população de \e[93m#{response[:nome]}\e[0m em 2019 era de
          \e[93m#{response[:population2019]}\e[0m habitantes."
    print_table(response, response[:rank])
  end

  def print_names_by_decades(response)
    # "response" is an array with the results for each name in names, which is a string
    rows, header = define_rows(response, '')
    puts "\n\e[0;4;7m#{'DÉCADAS'.rjust(10, ' ')}   #{header}\e[0m"
    rows.each_key { |decade| puts "#{decade.to_s.rjust(11, ' ')} #{rows[decade]}\n" }
  end

  private

  def define_rows(response, header)
    rows = {}
    response.each do |item| # item is a hash
      header += "|#{item[:nome].upcase.rjust(11, ' ')} "
      item[:res].each do |decade| # item[:res] is an array and decade is a hash
        s = " |#{decade[:frequencia].to_s.rjust(11, ' ')}"
        rows[decade[:periodo]] = rows[decade[:periodo]].to_s + s
      end
    end
    [rows, header]
  end

  def category_rank_gender_string(category)
    gender = ''
    gender += (category == :male ? ' masculinos' : ' femininos') unless category == :geral
    "\n\e[93mNomes\e[92m#{gender}\e[93m mais comuns\e[0m"
  end

  def percentage_frequency(name, total)
    relation = (name[:frequencia] / total.to_f) * 100
    "#{name[:frequencia]} (#{relation.round(2)}%)"
  end

  def print_table(locale, response)
    # Locale may be a state or a city
    response.each_key do |category|
      puts(category_rank_gender_string(category))
      puts("\e[0;4;7m RANK | #{'NOME'.rjust(11, ' ')} | FREQUÊNCIA (%) \e[0m")
      response[category][:res].each do |nome|
        freq = percentage_frequency(nome, locale[:population2019])
        puts "  #{nome[:ranking].to_s.rjust(2, '0')}  |#{nome[:nome].rjust(12, ' ')} | #{freq}"
      end
    end
  end
end
