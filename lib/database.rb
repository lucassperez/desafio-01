# frozen_string_literal: true

# Manages database, as if it was the active record, maybe?
class Database
  def where(table: nil, query: nil)
    return [] if table.nil? || query.nil?

    key = query.keys.first
    db = open_db
    ar = db.exec("SELECT * FROM #{table} WHERE #{key} = #{single_quote(query[key])}").to_a
    db.close
    ar.map { |item| symbolize_keys_result(item) }
  end

  def insert(table: nil, columns: nil)
    return nil if table.nil? || columns.nil?

    db = open_db
    db.exec(create_insert_statement(columns, table))
    db.close
    true
  end

  def delete(table: nil, query: nil)
    return nil if table.nil? || query.nil?

    key = query.keys.first
    db = open_db
    db.exec("DELETE FROM #{table} WHERE #{key} = #{single_quote(query[key])}")
    db.close
    true
  end

  def update(table: nil, columns: nil, query: nil)
    return nil if table.nil? || columns.nil? || query.nil?

    db = open_db
    db.exec(create_update_statement(columns, table, query))
    db.close
    true
  end

  def upsert(table: nil, columns: nil, query: nil)
    return nil if table.nil? || columns.nil?

    ar = columns.first
    query = { ar.first => ar.last } if query.nil?
    key = query.keys.first

    if where(table: table, query: { key => query[key] }).empty?
      insert(table: table, columns: columns)
    else
      update(table: table, columns: columns, query: query)
    end
    true
  end

  def get_table(table = nil)
    return nil if table.nil?

    db = open_db
    array = db.exec("SELECT * FROM #{table}").to_a
    db.close
    array.map { |item| symbolize_keys_result(item) }
  end

  private

  def open_db
    h = { host: ENV['DB_HOST'], user: ENV['POSTGRES_USER'], password: ENV['POSTGRES_PASSWORD'] }
    h[:dbname] = ENV['db'] == 'test' ? ENV['TEST_DB'] : ENV['POSTGRES_DB']

    PG::Connection.new(h)
  end

  def create_insert_statement(columns, table)
    keys = ''
    values = ''
    columns.each_key do |key|
      keys += "#{key},"
      values += "#{single_quote(columns[key])},"
    end
    "INSERT INTO #{table}(#{keys.chop}) VALUES (#{values.chop})"
  end

  def create_update_statement(columns, table, query)
    set = ''
    query_key = query.keys.first
    columns.each_key { |key| set += "#{key}=#{single_quote(columns[key])}," }
    "UPDATE #{table} SET #{set.chop} WHERE #{query_key} = #{query[query_key]}"
  end

  def single_quote(val)
    return "'#{val}'" if val.instance_of?(String) || val.instance_of?(Symbol)

    val.to_s
  end

  def symbolize_keys_result(hash)
    result = {}
    hash.each_key { |key| result[key.to_sym] = hash[key] }
    result
  end
end
