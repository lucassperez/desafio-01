# frozen_string_literal: true

# This class runs the application and prints the menu
class Application
  def run
    system('clear')
    puts("\n\t** Bem vindo à minha aplicação Ruby! **\n\n")
    puts('Você pode buscar os nomes mais comuns no Brasil segundo censos do IBGE!')

    while (choice = menu) != EXIT
      switch_case_menu(choice)
    end

    print("\e[91;1mObrigado por usar a minha aplicação! (:\n\e[0m")
  end

  def menu
    puts("\n\e[0mDigite um número para cada uma das opções seguintes:")
    puts("\e[32m[#{UF_LIST}] Listar todas as Unidades Federativas do Brasil (estados)\e[0m")
    puts("\e[32m[#{NAMES_BY_UF}] Consultar nomes mais comuns de um estado\e[0m")
    puts("\e[32m[#{NAMES_BY_CITY}] Consultar nomes mais comuns de uma cidade\e[0m")
    puts("\e[32m[#{NAMES_BY_DECADES}] Consultar frequência de um ou mais nomes ao longo das décadas\e[0m")
    puts("\e[95m[#{SAVE_RANK_IN_DB}] Salvar rank de nomes de cidades no banco de dados local\e[0m")
    puts("\e[93m[#{EXIT}] Sair\e[0m\n")
    print("\e[36m»» ")
    gets.to_i
  end

  def switch_case_menu(choice)
    case choice
    when UF_LIST then all_states
    when NAMES_BY_UF then names_by_state
    when NAMES_BY_CITY then names_by_city
    when NAMES_BY_DECADES then names_by_decades
    when SAVE_RANK_IN_DB then save_rank_db
    end
  end

  def all_states
    states = DBCheck.new.db_all_states
    Printer.new.print_all_states(states)
  end

  def names_by_state
    puts "\e[0mEntre com a sigla ou o ID de um estado: "
    print("\tVocê pode ver todos os estados e seus IDs com a opção 1 do menu principal\n\e[36m»»»» ")
    state = DBCheck.new.db_find_state(gets.strip)
    if state.empty?
      puts "\n\e[95mNão conseguimos localizar este estado. ):\e[0m"
      return nil
    end

    response = ApiCheck.new.rank_by_locale(state.first[:id])
    return nil if response.nil?

    Printer.new.print_names_by_state(state.first, response)
  end

  def names_by_city
    puts "\e[0mDigite o nome da cidade que você deseja buscar:"
    print("\e[36m»»»» ")
    city = DBCheck.new.db_find_city(gets.chomp)
    response = ApiCheck.new.names_by_city(city)
    return nil if response.nil?

    Printer.new.print_names_by_city(response)
  end

  def names_by_decades
    puts "\e[0mDigite um ou mais nomes separados por vírgula: "
    print("\e[36m»»»» ")
    names = gets.split(',').map(&:strip).join('%7C')
    return nil if names.empty?

    response = ApiCheck.new.names_by_decades(names)
    return nil if response.nil?

    Printer.new.print_names_by_decades(response)
  end

  def save_rank_db
    puts "\e[1;95mEntre com a sigla, nome ou o ID de um estado: "
    puts "\e[0m\tO id pode ser verificado com a opção 1 do menu\e[1;95m"
    print('»»»» ')
    state = DBCheck.new.db_find_state(gets.strip) # returns an array
    if state.empty?
      puts "\nNão conseguimos localizar este estado. ):\e[0m"
      return nil
    end

    make_city_rank_asyncs(state)
  end

  def make_city_rank_asyncs(state)
    puts "O estado escolhido foi #{state.first[:nome]}"
    puts "\e[7mVocê tem certeza?\e[0m [s/n]"
    confirm = gets.strip
    return nil if confirm.upcase != 'S'

    cities = ApiCheck.new.all_cities_of_a_state(state.first[:id])
    cities.each { |city| SaveCityRankWorker.perform_async(city[:id]) }
  end
end
