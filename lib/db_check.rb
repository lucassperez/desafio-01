# frozen_string_literal: true

# This class visits the Database and return the results
class DBCheck
  # before_action :db ? :( Talvez o initialize funcionasse como before_action

  def db_all_states
    db.get_table('states')
  end

  def db_find_city(city)
    city_name = city.split('/').first.strip.upcase
    db.where(table: 'cities', query: { 'upper(nome)' => city_name })
  end

  def db_find_state(state)
    if number?(state)
      column = 'id' if number?(state)
    else
      column = state.size == 2 ? 'sigla' : 'upper(nome)'
    end
    query = column == 'id' ? state.to_s : state.upcase
    db.where(table: 'states', query: { column => query.strip })
  end

  private

  def db
    Database.new
  end

  def number?(string)
    # Returns 0 if string is a whole number and false otherwise
    string =~ /^[0-9]+$/
  end
end
