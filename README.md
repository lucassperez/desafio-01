# Desafio 01 Pós Treina Dev - Turma 04

Neste desafio, está sendo criada uma aplicação ruby puro para
usar no terminal capaz de verificar quais os nomes são mais comuns
no Brasil, podendo olhar por estado, cidade, e ainda checar a
frequência (proporcional) de algum nome específico.

## Pré-requisitos
Esta aplicação foi desenvolvida usando Ruby na sua versão **2.7.1**

## Gems
Estão sendo utilizadas as seguintes gems:
- **faraday**, para visitar as APIs
- **json**, para manipular JSONs
- **rubocop**, para verificar a formatação
- **rspec**, para realizar testes
- **simplecov**, para verificar a cobertura que os testes têm sobre a aplicação

## Funcionalidades
Com esta aplicação é possível verificar os 20 nomes mais frequentes de um determinado estado brasileiro ou de uma determinada cidade brasileira, além de ver a frequência de um nome específico ao longo das últimas décadas.\
Todas as informações usadas foram disponibilizadas pelo próprio IBGE em suas APIs de localidades e nomes.
### API de Localidades:
https://servicodados.ibge.gov.br/api/docs/localidades?versao=1
### API de Nomes:
https://servicodados.ibge.gov.br/api/docs/censos/nomes?versao=2

Visite também a versão a aplicação do IBGE de nomes! (:\
https://ibge.gov.br/censo2010/apps/nomes/#/search

### Salvar Rank em Banco de Dados Local
Também existe a opção de salvar o rank de nomes de cidades no banco de dados local.\
Essa funcionalidade deve rodar assíncronamente, ou seja, enquanto as informações são gravadas,\
processo que pode demorar alguns minutos, você pode continuar usando a aplicação se quiser.\
Para mais informações em como funciona isso, continue para a seção **Instalando e usando**.

## Instalando e usando
Esta aplicação faz uso do Docker e docker-compose. Após clonar o repositório, devemos
executar o comando:

    docker-compose build
E agora ele construirá as imagens necessárias do Docker\
Para executar a aplicação, execute:

    docker-compose run --rm app bash
Isso nos colocará num shell dentro do Docker, onde podemos finalmente executar agora o comando:

    bin/setup
Depois que a instalação terminar, para rodar o programa basta executar o comando:

    ./run
Ou, alternativamente:

    ruby lib/run.rb
---
**Vale notar que** se executarmos o comando

    docker-compose run --rm app ./run
é como se estivéssemos entrando naquele shell dentro do Docker e depois executando o `./run`,
ou seja, ele vai abrir o programa direto.

## Testes
A aplicação conta com testes automatizados para todas suas funções e funcionalidades.\
Para começar a rotina de testes, podemos primeiro entrar no Docker:

    docker-compose run --rm app bash
Isso nos coloca numa linha de comandos dentro do container do Docker.\
Agora, basta executar:

    rspec
na pasta raíz do projeto.

---
Alternativamente, também podemos simplesmente executar:

    docker-compose run --rm app rspec

Porém, para começar os testes é necessário ter o banco de dados já criado antes.\
Se este não for o caso, o `bin/setup` cria o banco, mas para executá-lo devemos primeiro estar dentro do container do Docker. Veja a seção "Instalando e usando" acima.

## Salvar Rank em Banco de Dados Local
Para fazer isso, é necessário inicializar os serviços do redis e do sidekiq uma vez dentro do Docker.\
**Mas atenção!** \
Ao iniciar o sidekiq, você não poderá usar aquele terminal. Então você terá que ter **dois** terminais abertos,\
um para a aplicação e outro para o sidekiq.\

Para entrar no docker, execute o comando:

    docker-compose run --rm app bash

**Nesse momento eu aconselho que você copie o id do container, pois precisaremos nos conectar com ele depois!** \
O id do container fica logo após o nome do usuário. Na linha de comando provavelmente deve aparecer algo parecido\
com o seguinte:

    root@ac04020b26bb:/ibge#

O id seria essa sequência de números e letras **entre o @ e o :**, nesse **exemplo**, **ac04020b26bb**. \
\
Note que este é apenas um exemplo, o id gerado em seu computador deve ser bem diferente. Copie e anote o id que\
aparece para você!\
\
Para inicializar o serviço do _redis_, execute:

    redis-server --daemonize yes

Esse comando inicia o servidor do redis _em brackground!_
Se quiser testar o serviço, execute

    redis-cli ping

E o output esperado é

    PONG

E agora, para iniciar o _sidekiq_, execute o comando:

    bundle exec sidekiq -r ./lib/jobs/save_city_rank.rb

Neste momento você deve ver uma "tela inicial" do sidekiq, confirmando que o serviço foi iniciado.\
![Image of Sidekiq welcome screen](/assets/sidekiq-intro.png)\
Caso queira sair, aperte CONTROL+C no teclado.

Para usar a funcionalidade de **Salvar Rank em Banco de Dados Local** agora é necessário abrir um _segundo_ terminal\
onde poderemos rodar a aplicação normalmente.\
Vamos precisar entrar no _mesmo container em que o sidekiq e o redis estão rodando_!
Para isso, usamos o **id** do container que está aberto, que foi anotado primeiro passo dessas instruções :)
___
    Caso você não tenha o id do container, você pode parar o sidekiq usando o comando CONTROL+C e verificá-lo na linha
    de comando. O id do container deve aparecer após o usuário, como por exemplo aqui:

        root@ac04020b26bb:/ibge#

    Neste caso, o id é ac04020b26bb.
    Volte colocar o sidekiq em pé caso você o tenha parado.
___

Após abrir um segundo terminal, execute:

    docker exec -it [ID] bash

Onde [ID] é o id do container.\
**No _EXEMPLO_ anterior ficaríamos com algo assim:**

    docker exec -it ac04020b26bb bash

Uma vez dentro do container de mesmo id, você pode executar o programa\
normalmente com o seguinte comando na pasta raíz da aplicação:

    ./run

Selecione a opção número 9 do menu e siga as instruções.\
Você deve escolher um **estado** brasileiro e a aplicação vai preencher o banco de dados local de\
maneira **assíncrona** com o rank de nomes de\
**TODAS as cidades do estado escolhido!** \
Esse processo pode demorar alguns minutos caso o estado tenha muitas cidades, mas enquanto isso está sendo feito\
você pode continuar usando a aplicação normalmente, sem ter que esperar nada.\
Inclusive você pode colocar outro estado na fila! Assim que o sidekiq termine de processar todas as cidades do\
primeiro estado, ele já vai começar a preencher o banco com todas as cidades deste outro estado na sequência!\
No terminal onde estamos rodando o sidekiq é possível ver algumas mensagens toda vez que um rank é gravado\
em seu respectivo banco.

Ao terminar de usar a aplicação, é recomendado parar o **sidekiq** (CONTROL+C) e também o **redis**.\
Para parar o serviço do redis, execute:

    redis-cli shutdown

Para testar se o serviço foi parado corretamente, o comando

    redis-cli ping

Não deve mais retornar PONG, e sim algum tipo de mensagem de falha ao tentar conectar com o redis.
