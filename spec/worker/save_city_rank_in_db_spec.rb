# frozen_string_literal: true

require 'faraday'
require 'sidekiq'
require 'spec_helper'

require_relative '../../lib/jobs/save_city_rank'
require_relative '../test_db'

describe 'Worker#save_city_rank_in_db', :db do
  geral = File.read('spec/support/names_by_city/geral_abelardo_luz.json')

  it 'successfully' do
    dbl = double(status: 200, body: geral)
    allow(Faraday).to receive(:get).and_return(dbl)

    SaveCityRankWorker.new.perform(4_200_101) # 4200101 is the id of the city Abelardo Luz
    result = Database.new.where(table: 'cities_rank_geral', query: { city_id: '4200101' })

    expect(result.first[:name7]).to eq('LUCAS')
  end
end
