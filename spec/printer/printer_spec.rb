# frozen_string_literal: true

require 'faraday'
require 'json'
require 'spec_helper'

describe 'Printer' do
  let(:printer) { Printer.new }

  it 'print_all_states' do
    states = JSON.parse(File.read('spec/support/states.json'), symbolize_names: true)

    expect { printer.print_all_states(states) }.to output(a_string_including('Santa Catarina')).to_stdout
    expect { printer.print_all_states(states) }.to_not output(a_string_including('Florianópolis')).to_stdout
    expect { printer.print_all_states(states) }.to_not output(a_string_including('MARIA')).to_stdout
  end

  it 'print_names_by_state' do
    state = { id: 42, nome: 'Santa Catarina', sigla: 'SC', population2019: 1_000_000 }
    json = JSON.parse(File.read("spec/support/name_by_state/#{state[:id]}.json"), symbolize_names: true)
    response = { geral: json[0], male: json[0], fem: json[0] }

    expect { printer.print_names_by_state(state, response) }.to output(a_string_including('MARIA')).to_stdout
    expect { printer.print_names_by_state(state, response) }.to output(a_string_including('JOAO')).to_stdout
    expect { printer.print_names_by_state(state, response) }.to output(a_string_including('Santa Catarina')).to_stdout
    expect { printer.print_names_by_state(state, response) }.to_not output(a_string_including('Rondônia')).to_stdout
  end

  it 'print_names_by_decades' do
    allow($stdin).to receive(:gets).and_return('lucas, valentina, julia')
    response = JSON.parse(File.read('spec/support/names_by_decades/multiple_names.json'), symbolize_names: true)

    expect { printer.print_names_by_decades(response) }.to output(a_string_including('LUCAS')).to_stdout
    expect { printer.print_names_by_decades(response) }.to_not output(a_string_including('MARIA')).to_stdout
  end
end
