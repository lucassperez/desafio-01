# frozen_string_literal: true

require 'json'
require 'spec_helper'

describe 'Printer' do
  it '#print_names_by_city' do
    geral = JSON.parse(File.read('spec/support/names_by_city/geral_abelardo_luz.json'), symbolize_names: true)
    male = JSON.parse(File.read('spec/support/names_by_city/male_abelardo_luz.json'), symbolize_names: true)
    female = JSON.parse(File.read('spec/support/names_by_city/fem_abelardo_luz.json'), symbolize_names: true)
    rank_by_locale = { geral: geral[0], male: male[0], fem: female[0] }
    city_info = { id: 4_200_101, nome: 'Abelardo Luz' }
    response = { **city_info, rank: rank_by_locale, population2019: 1_000_000 }

    expect { Printer.new.print_names_by_city(response) }
      .to output(a_string_including('07  |       LUCAS')).to_stdout
    expect { Printer.new.print_names_by_city(response) }
      .to output(a_string_including('Abelardo Luz')).to_stdout
  end
end
