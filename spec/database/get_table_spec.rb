# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#get_table method', :db do
  it 'successfully gets a table' do
    db.insert(table: 'cities', columns: { id: 4_200_051, nome: 'Abdon Batista' })
    db.insert(table: 'cities', columns: { id: 4_200_101, nome: 'Abelardo Luz' })

    result = db.get_table('cities')

    expect(result.size).to eq(2)
    expect(result[0][:nome]).to eq('Abdon Batista')
    expect(result[1][:nome]).to eq('Abelardo Luz')
    expect(result[1][:id]).to eq('4200101')
    expect(result[0][:nome]).to_not eq('Florianópolis')
  end

  it 'no table passed returns nil' do
    expect(db.get_table).to eq(nil)
  end
end
