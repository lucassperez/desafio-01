# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#update method', :db do
  it 'successfully updates a city' do
    db.insert(table: 'cities', columns: { id: 123, nome: 'Cidade Teste' })

    db.update(table: 'cities', columns: { id: 999, nome: 'Novo Nome' }, query: { id: 123 })
    result = db.where(table: 'cities', query: { id: 999 })

    expect(db.where(table: 'cities', query: { id: 123 })).to be_empty
    expect(result.first[:nome]).to eq('Novo Nome')
    expect(result.first[:id]).to eq('999')
    expect(result.first[:id]).to_not eq('123')
  end

  it 'fails at updating a city' do
    db.insert(table: 'cities', columns: { id: 123, nome: 'Cidade Teste' })

    db.update(table: 'cities', columns: { nome: 'Novo Nome' })
    result = db.where(table: 'cities', query: { id: 123 })

    expect(result.first[:nome]).to eq('Cidade Teste')
  end
end
