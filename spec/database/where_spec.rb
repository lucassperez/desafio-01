# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#where method', :db do
  it 'queries by name and returns one city' do
    db.insert(table: 'cities', columns: { id: 4_200_051, nome: 'Abdon Batista' })
    db.insert(table: 'cities', columns: { id: 4_200_101, nome: 'Abelardo Luz' })

    result = db.where(table: 'cities', query: { nome: 'Abdon Batista' })

    expect(result.size).to eq(1)
    expect(result[0][:nome]).to include('Abdon Batista')
    expect(result[0][:nome]).to_not include('Abelardo Luz')
  end

  it 'queries by id and return one city' do
    db.insert(table: 'cities', columns: { id: 4_200_051, nome: 'Abdon Batista' })
    db.insert(table: 'cities', columns: { id: 4_200_101, nome: 'Abelardo Luz' })

    result = db.where(table: 'cities', query: { id: 4_200_051 })

    expect(result.size).to eq(1)
    expect(result[0][:id]).to eq('4200051') # pg returns strings even if the column's data type is intenger
    expect(result[0][:nome]).to_not include('Abelardo Luz')
  end

  it 'returns nothing' do
    result = db.where

    expect(result.size).to eq(0)
    expect(result).to eq([])
  end
end
