# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#insert method', :db do
  it 'successfully deletes a city' do
    db.insert(table: 'cities', columns: { id: 4_200_051, nome: 'Abdon Batista' })
    db.insert(table: 'cities', columns: { id: 4_200_101, nome: 'Abelardo Luz' })

    delete_result = db.delete(table: 'cities', query: { nome: 'Abdon Batista' })
    find_city = db.where(table: 'cities', query: { nome: 'Abelardo Luz' })
    dont_find = db.where(table: 'cities', query: { nome: 'Abdon Batista' })

    expect(delete_result).to eq(true)
    expect(find_city.size).to eq(1)
    expect(dont_find.size).to eq(0)
  end

  it 'fails at deleting a city' do
    db.insert(table: 'cities', columns: { id: 1, nome: 'Nome Certo' })

    db.delete(table: 'cities', query: { nome: 'Nome Errado' })
    find_city = db.where(table: 'cities', query: { id: 1 })

    expect(find_city.size).to eq(1)
    expect(find_city[0][:nome]).to eq('Nome Certo')
  end
end
