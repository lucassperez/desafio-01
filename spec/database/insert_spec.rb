# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#insert method', :db do
  it 'successfully inserts a city' do
    expect(db.insert(table: 'cities', columns: { id: 123, nome: 'Cidade Teste' })).to eq(true)
    expect(db.where(table: 'cities', query: { id: 123 })[0][:nome]).to eq('Cidade Teste')
  end

  it 'fails at inserting a city' do
    result = db.insert(table: 'cities')

    expect(result).to eq(nil)
  end
end
