# frozen_string_literal: true

require 'spec_helper'

require_relative '../test_db'

db = Database.new
describe 'Database#upsert method', :db do
  it 'successfully updates a city' do
    db.insert(table: 'cities', columns: { id: 123, nome: 'Cidade Teste' })

    db.upsert(table: 'cities', columns: { nome: 'Novo Nome' }, query: { id: 123 })
    result = db.where(table: 'cities', query: { id: 123 })

    expect(result.size).to eq(1)
    expect(result[0][:nome]).to include('Novo Nome')
    expect(result[0][:id]).to include('123')
    expect(result[0][:nome]).to_not include('Cidade Teste')
  end

  it 'successfully inserts a city' do
    db.insert(table: 'cities', columns: { id: 123, nome: 'Cidade Teste' })

    db.upsert(table: 'cities', columns: { id: 456, nome: 'Outra Cidade' })
    result = db.where(table: 'cities', query: { id: 456 })

    expect(result.size).to eq(1)
    expect(result[0][:nome]).to include('Outra Cidade')
    expect(result[0][:nome]).to_not include('Cidade Teste')
  end

  it 'fails at inserting a city' do
    result = db.upsert(table: 'cities')

    expect(result).to eq(nil)
  end
end
