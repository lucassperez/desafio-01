# frozen_string_literal: true

ENV['db'] ||= 'test'

require 'active_support/all'
require 'simplecov'

require_relative 'test_db'

SimpleCov.start

PROJECT_ROOT = File.expand_path('..', __dir__)

Dir.glob(File.join(PROJECT_ROOT, 'lib', '*.rb')).each do |file|
  autoload File.basename(file, '.rb').camelize, file
end

RSpec.configure do |config|
  config.before(:each, db: true) do
    reset_test_database
  end

  config.after(:each, db: true) do
    reset_test_database
  end
end
