# frozen_string_literal: true

# I don't know why but if I don't require 'db_check', it just
# does not work :(
require 'db_check'
require 'json'
require 'spec_helper'

require_relative '../test_db'

describe 'DBCheck', :db do
  it '#db_all_states' do
    rondonia = JSON.parse(File.read('spec/support/states.json'), symbolize_keys: true).first
    db = Database.new
    db.insert(table: 'states', columns: rondonia)

    response = DBCheck.new.db_all_states

    expect(response.size).to eq(1)
    expect(response[0][:id]).to eq('11')
    expect(response[0][:nome]).to eq('Rondônia')
    expect(response[0][:sigla]).to eq('RO')
  end
end
