# frozen_string_literal: true

require 'db_check' # why do I have to require here again???
require 'json'
require 'spec_helper'

require_relative '../test_db'

describe 'DBCheck', :db do
  db = Database.new
  states_json = JSON.parse(File.read('spec/support/states.json'), symbolize_keys: true)

  it 'find state by initials' do
    states_json.each { |state| db.insert(table: 'states', columns: state) }
    result = DBCheck.new.db_find_state('RO')

    expect(result.size).to eq(1)
    expect(result[0][:id]).to eq('11')
    expect(result[0][:nome]).to eq('Rondônia')
    expect(result[0][:sigla]).to eq('RO')
  end

  it 'find state by id' do
    states_json.each { |state| db.insert(table: 'states', columns: state) }
    result = DBCheck.new.db_find_state('11')

    expect(result[0][:id]).to eq('11')
    expect(result[0][:nome]).to eq('Rondônia')
  end

  it 'does not find state' do
    states_json.each { |state| db.insert(table: 'states', columns: state) }
    result = DBCheck.new.db_find_state('999')

    expect(result).to eq([])
  end
end
