# frozen_string_literal: true

require 'db_check'
require 'json'
require 'spec_helper'

require_relative '../test_db'

describe 'DBCheck#db_find_city', :db do
  db = Database.new
  cities_json = JSON.parse(File.read('spec/support/cities.json'), symbolize_keys: true)

  it 'find city with state initials and case insensitive' do
    cities_json.each { |city| db.insert(table: 'cities', columns: city) }
    result = DBCheck.new.db_find_city('AbdON BatIsta/SC')

    expect(result.size).to eq(1)
    expect(result[0][:nome]).to eq('Abdon Batista')
  end

  it 'does not find city' do
    cities_json.each { |city| db.insert(table: 'cities', columns: city) }
    result = DBCheck.new.db_find_city('CidadeedadiC')

    expect(result).to eq([])
  end
end
