# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck#find_state_id' do
  # Define some variables before
  json = File.read('spec/support/states.json')
  let(:duble) { double(status: 200, body: json) }

  it 'successfully' do
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.find_state_id('RO')

    expect(response).to eq(11)
  end

  it 'is case insensitive' do
    allow(Faraday).to receive(:get).and_return(duble)
    response1 = ApiCheck.new.find_state_id('ro')
    response2 = ApiCheck.new.find_state_id('aC')
    response3 = ApiCheck.new.find_state_id('Ro')

    expect(response1).to eq(11)
    expect(response2).to eq(12)
    expect(response3).to eq(11)
  end

  it 'unsuccessfully' do
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.find_state_id('AM')

    expect(response).to eq(nil)
  end
end
