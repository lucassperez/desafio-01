# frozen_string_literal: true

require 'faraday'
require 'json'
require 'spec_helper'

describe 'ApiCheck#find_city' do
  # Define some variables before
  json = File.read('spec/support/cities.json')
  let(:duble) { double(status: 200, body: json) }

  it 'successfully and case insensitive' do
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.find_city('abdon BatIsTa')

    expect(response[:id]).to eq(4_200_051)
    expect(response[:nome]).to eq('Abdon Batista')
  end

  it 'with state' do
    states_json = File.read('spec/support/states.json')
    states_response = double(status: 200, body: states_json)
    allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
                                   .and_return(states_response)
    allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/42/municipios')
                                   .and_return(duble)
    response = ApiCheck.new.find_city('Abdon Batista/SC')

    expect(response[:id]).to eq(4_200_051)
    expect(response[:nome]).to eq('Abdon Batista')
  end

  it 'unsuccessfully' do
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.find_city('Abdoon Batistazzz')

    expect(response).to eq(nil)
  end
end
