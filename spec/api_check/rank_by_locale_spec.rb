# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck#rank_by_locale' do
  state = 42 # Could be any, but my JSON support file has 42 in it

  it 'geral successfully' do
    json = File.read("spec/support/name_by_state/#{state}.json")
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.rank_by_locale(state)[:geral]

    expect(response[:sexo]).to eq(nil)
    expect(response[:res][0][:nome]).to eq('MARIA')
    expect(response[:res][1][:nome]).to eq('JOAO')
    expect(response[:res][0][:ranking]).to eq(1)
  end

  it 'male successfully' do
    json = File.read("spec/support/name_by_state/#{state}_male.json")
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.rank_by_locale(state)[:male]

    expect(response[:sexo]).to eq('m')
  end

  it 'female successfully' do
    json = File.read("spec/support/name_by_state/#{state}_female.json")
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.rank_by_locale(state)[:fem]

    expect(response[:sexo]).to eq('f')
  end
end
