# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck#names_by_city' do
  it 'successfully' do
    geral = File.read('spec/support/names_by_city/geral_abelardo_luz.json')
    dbl = double(status: 200, body: geral)
    allow(Faraday).to receive(:get).and_return(dbl)
    city = [{ id: 4_200_101, nome: 'Abelardo Luz' }]

    response = ApiCheck.new.names_by_city(city)

    expect(response[:rank][:geral][:res].first[:nome]).to eq('MARIA')
    expect(response[:rank][:fem][:res].first[:nome]).to_not eq('LUCAS')
  end

  it 'does not find city' do
    city = []

    expect { ApiCheck.new.names_by_city(city) }
      .to output(a_string_including('Não conseguimos localizar esta cidade')).to_stdout
  end
end
