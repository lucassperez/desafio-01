# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck' do
  it '#list_all_states' do
    json = File.read('spec/support/states.json')
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)

    response = ApiCheck.new.list_all_states

    # expect(response.size).to eq(1)
    expect(response[0][:id]).to eq(11)
    expect(response[0][:nome]).to eq('Rondônia')
    expect(response[0][:sigla]).to eq('RO')
    expect(response[1][:sigla]).to_not eq('SP')
  end
end
