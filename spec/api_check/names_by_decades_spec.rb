# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck#names_by_decades' do
  it 'single name' do
    json = File.read('spec/support/names_by_decades/single_name.json')
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.names_by_decades('lucas').first # first because it is a single name

    expect(response[:res][0].value?('1930[')).to eq(true)
    expect(response[:res][1][:frequencia]).to eq(937)
    expect(response[:nome]).to eq('LUCAS')
    expect(response.key?('VALENTINA')).to eq(false)
  end

  it 'multiple names' do
    json = File.read('spec/support/names_by_decades/multiple_names.json')
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.names_by_decades('lucas%7Cvalentina%7Cjulia')

    expect(response.length).to eq(3)
    expect(response[0][:nome]).to eq('JULIA')
    expect(response[1][:nome]).to eq('LUCAS')
    expect(response[2][:nome]).to eq('VALENTINA')
    expect(response[2][:res][0][:periodo]).to eq('1930[')
    expect(response[2][:res][1][:frequencia]).to eq(646)
  end
end
