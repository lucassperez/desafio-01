# frozen_string_literal: true

require 'faraday'
require 'spec_helper'

describe 'ApiCheck#all_cities_of_a_state' do
  let(:json) { File.read('spec/support/cities.json') }

  it 'successfully' do
    duble = double(status: 200, body: json)
    allow(Faraday).to receive(:get).and_return(duble)

    response = ApiCheck.new.all_cities_of_a_state(42)

    expect(response.size).to eq(3)
    expect(response.first[:nome]).to eq('Abdon Batista')
  end

  it 'unsuccessfully' do
    duble = double(status: 418)
    allow(Faraday).to receive(:get).and_return(duble)
    response = ApiCheck.new.all_cities_of_a_state('')

    expect(response).to eq(['Erro'])
  end
end
