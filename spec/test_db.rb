# frozen_string_literal: true

require 'pg'

def reset_test_database
  test = PG::Connection.new(host: ENV['DB_HOST'],
                            user: ENV['POSTGRES_USER'],
                            password: ENV['POSTGRES_PASSWORD'],
                            dbname: ENV['TEST_DB'])
  test.exec('TRUNCATE states')
  test.exec('TRUNCATE cities')
  %w[geral male fem].each { |category| test.exec("TRUNCATE cities_rank_#{category}") }
  test.close
end
